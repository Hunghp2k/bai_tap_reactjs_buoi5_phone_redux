import React, { Component } from 'react'
import CartPhone from './CartPhone'
import { data_phone } from './dataPhone'
import DetailPhone from './DetailPhone'
import ListPhone from './ListPhone'

export default class Ex_Phone extends Component {

    render() {
        return (
            <div>
                <CartPhone />
                <ListPhone />
                <DetailPhone />
            </div>
        )
    }
}
