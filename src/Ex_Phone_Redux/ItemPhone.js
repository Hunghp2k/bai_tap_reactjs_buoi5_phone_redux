import React, { Component } from 'react'
import { connect } from 'react-redux';

class ItemPhone extends Component {
    render() {
        let { hinhAnh, giaBan, tenSP } = this.props.data
        return (
            <div className='col-4 p-4'>
                <div className="card border-primary">
                    <img className="card-img-top" src={hinhAnh} alt />
                    <div className="card-body">
                        <h4 className="card-title">{tenSP}</h4>
                        <p className="card-text">{giaBan}</p>
                        <button onClick={() => { this.props.detailSP(this.props.data) }} className='btn btn-success'>Xem Chi Tiết</button>
                        <button onClick={() => { this.props.addSP(this.props.data) }} className='btn btn-danger'>Add</button>
                    </div>

                </div>
            </div>
        )
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        detailSP: (payload) => {
            let action = {
                type: "XEM_CHI_TIET",
                payload: payload,
            };
            dispatch(action)
        },
        addSP: (SP) => {
            let action = {
                type: "ADD",
                payload: SP,
            };
            dispatch(action)
        },
    };
};
export default connect(null, mapDispatchToProps)(ItemPhone)
