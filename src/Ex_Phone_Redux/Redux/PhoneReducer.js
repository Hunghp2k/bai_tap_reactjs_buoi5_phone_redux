import { data_phone } from "../dataPhone";


let initiaValue = {
    listPhone: data_phone,
    detail: data_phone[0],
    cart: [],
};


export const phoneReducer = (state = initiaValue, action) => {
    switch (action.type) {
        case "ADD": {
            let clonecart = [...state.cart];
            let index = clonecart.findIndex((item) => {
                return item.maSP == action.payload.maSP
            });
            if (index == -1) {
                let newcart = { ...action.payload, soLuong: 1 };
                clonecart.push(newcart);
            } else {
                clonecart[index].soLuong++;
            }
            return { ...state, cart: clonecart }
        }
        case "XOASP": {
            let newCart = state.cart.filter((item) => {
                return item.maSP != action.payload
            })
            return { ...state, cart: newCart };
        }
        case "TANGGIAM": {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => {
                return item.maSP == action.payload.id;
            });
            cloneCart[index].soLuong += action.payload.luachon;
            return { ...state, cart: cloneCart }
        }

        case "XEM_CHI_TIET": {

            return { ...state, detail: action.payload }
        }

        default: {
            return { ...state }
        }
    }
}