import { combineReducers } from "redux"
import { phoneReducer } from "./PhoneReducer"

export const rootReducer_Ex_Phone = combineReducers({
    phoneReducer,
})