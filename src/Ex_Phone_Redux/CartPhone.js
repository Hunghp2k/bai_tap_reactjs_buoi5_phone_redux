import { type } from '@testing-library/user-event/dist/type'
import React, { Component } from 'react'
import { connect } from 'react-redux'

class CartPhone extends Component {
    rendertbody = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr key={index}>
                    <th>{item.maSP}</th>
                    <th>{item.tenSP}</th>
                    <th>{item.giaBan}</th>
                    <th>
                        <img style={{ width: 50 }} src={item.hinhAnh} alt="" />
                    </th>
                    <th>
                        <button onClick={() => { this.props.TangGiam(item.maSP, -1) }} className='btn btn-primary'>-</button>
                        <strong>{item.soLuong}</strong>
                        <button onClick={() => { this.props.TangGiam(item.maSP, +1) }} className='btn btn-success'>+</button>
                    </th>
                    <th>{item.giaBan * item.soLuong}</th>
                    <th>
                        <button onClick={() => { this.props.XoaSP(item.maSP) }} className='btn btn-danger'>Xóa</button>
                    </th>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <div className='container'>
                    <h2>Phone Cart</h2>
                    <table className='table'>
                        <thead>
                            <tr>
                                <th>Mã sp</th>
                                <th>Tên sp</th>
                                <th>Giá Bán</th>
                                <th>Hình Ảnh</th>
                                <th>Số Lượng</th>
                                <th>Tổng tiền</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.rendertbody()}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { cart: state.phoneReducer.cart }
}
const mapDispatchToProps = (dispatch) => {
    return {
        XoaSP: (maSP) => {
            let action = {
                type: "XOASP",
                payload: maSP,
            }
            dispatch(action)
        },
        TangGiam: (maSP, luachon) => {
            let action = {
                type: "TANGGIAM",
                payload: { id: maSP, luachon },
            }
            dispatch(action)
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(CartPhone)