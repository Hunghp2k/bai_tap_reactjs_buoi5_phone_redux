import React, { Component } from 'react'
import { connect } from 'react-redux';
import ItemPhone from './ItemPhone';

class ListPhone extends Component {
    renderListPhone = () => {
        return this.props.list.map((item, index) => {
            return <ItemPhone data={item} key={index} />
        });
    };
    render() {
        return (
            <div className='row'>
                {this.renderListPhone()}
            </div>
        )
    }
}
let mapStateToProps = (state) => {
    return {
        list: state.phoneReducer.listPhone,
    }
}
export default connect(mapStateToProps)(ListPhone)

